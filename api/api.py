from flask import Flask, request
from flask_restful import Resource, Api
from flask_cors import CORS 

from lxml import html
import requests
import json

app = Flask(__name__)
api = Api(app)

# URL Pattern Example: ire:asx
class StockDetailFT(Resource):
    def get(self, symbol):
        page = requests.get('https://markets.ft.com/data/equities/tearsheet/forecasts?s={symbol}'.format(symbol=symbol))
        tree = html.fromstring(page.content)

        price = tree.xpath('/html/body/div[3]/div[2]/section[1]/div/div/div[1]/div[3]/ul/li[1]/span[2]/text()')
        description = tree.xpath('/html/body/div[3]/div[2]/section[1]/div/div/div[1]/div[1]/h1[1]/text()')
        consensus = tree.xpath('/html/body/div[3]/div[2]/section[3]/div[1]/div/div/section[1]/span/text()')
        forecast = tree.xpath('/html/body/div[3]/div[2]/section[3]/div[2]/div/div/section[1]/div/span/text()')
        recommendations_buy = tree.xpath('/html/body/div[3]/div[2]/section[3]/div[1]/div/div/section[2]/div[2]/table/tbody/tr[1]/td[3]/text()')
        recommendations_outperform = tree.xpath('/html/body/div[3]/div[2]/section[3]/div[1]/div/div/section[2]/div[2]/table/tbody/tr[2]/td[3]/text()')
        recommendations_hold = tree.xpath('/html/body/div[3]/div[2]/section[3]/div[1]/div/div/section[2]/div[2]/table/tbody/tr[3]/td[3]/text()')
        recommendations_underperform = tree.xpath('/html/body/div[3]/div[2]/section[3]/div[1]/div/div/section[2]/div[2]/table/tbody/tr[4]/td[3]/text()')
        recommendations_sell = tree.xpath('/html/body/div[3]/div[2]/section[3]/div[1]/div/div/section[2]/div[2]/table/tbody/tr[5]/td[3]/text()')
        forecast_high = tree.xpath('/html/body/div[3]/div[2]/section[3]/div[2]/div/div/section[2]/div[2]/table/tbody/tr[1]/td[3]/text()')
        forecast_median = tree.xpath('/html/body/div[3]/div[2]/section[3]/div[2]/div/div/section[2]/div[2]/table/tbody/tr[2]/td[3]/text()')
        forecast_low = tree.xpath('/html/body/div[3]/div[2]/section[3]/div[2]/div/div/section[2]/div[2]/table/tbody/tr[3]/td[3]/text()')

        return {
            'symbol': symbol,
            'price': price[0] if price else None,
            'description': description[0] if description else None,
            'consensus': consensus[0] if consensus else None,
            'forecast': ''.join(forecast) if forecast else None,
            'recommendations': {
                'buy': recommendations_buy[0] if recommendations_buy else None,
                'outperform': recommendations_outperform[0] if recommendations_outperform else None,
                'hold': recommendations_hold[0] if recommendations_hold else None,
                'underperform': recommendations_underperform[0] if recommendations_underperform else None,
                'sell': recommendations_sell[0] if recommendations_sell else None,
            },
            'forecasts': {
                "high": forecast_high[0] if forecast_high else None,
                "median": forecast_median[0] if forecast_median else None,
                "low": forecast_low[0] if forecast_low else None
            }
        }


# URL Pattern Example: ire:au
class StockDetailBloomberg(Resource):
    def get(self, symbol):
        page = requests.get('https://www.bloomberg.com/quote/{symbol}'.format(symbol=symbol))
        tree = html.fromstring(page.content)

        open_price = tree.xpath('//*[@id="content"]/div/div/div[8]/div/div/div[1]/div[2]/text()')
        day_range = tree.xpath('//*[@id="content"]/div/div/div[8]/div/div/div[2]/div[2]/text()')
        volume = tree.xpath('//*[@id="content"]/div/div/div[8]/div/div/div[3]/div[2]/text()')
        previous_close = tree.xpath('//*[@id="content"]/div/div/div[8]/div/div/div[4]/div[2]/text()')
        week_range = tree.xpath('//*[@id="content"]/div/div/div[8]/div/div/div[5]/div[2]/text()')
        one_year_return = tree.xpath('//*[@id="content"]/div/div/div[8]/div/div/div[6]/div[2]/text()')
        ytd_return = tree.xpath('//*[@id="content"]/div/div/div[8]/div/div/div[7]/div[2]/text()')
        pe_ratio = tree.xpath('//*[@id="content"]/div/div/div[8]/div/div/div[8]/div[2]/text()')
        earnings = tree.xpath('//*[@id="content"]/div/div/div[8]/div/div/div[9]/div[2]/text()')
        market_cap = tree.xpath('//*[@id="content"]/div/div/div[8]/div/div/div[10]/div[2]/text()')
        outstanding_shares = tree.xpath('//*[@id="content"]/div/div/div[8]/div/div/div[11]/div[2]/text()')
        price_sales = tree.xpath('//*[@id="content"]/div/div/div[8]/div/div/div[12]/div[2]/text()')
        div_indic_gross_yield = tree.xpath('//*[@id="content"]/div/div/div[8]/div/div/div[13]/div[2]/text()')

        return {
            "open_price": open_price[0].strip() if open_price else None,
            "day_range": day_range[0].strip() if day_range else None,
            "volume": volume[0].strip() if volume else None,
            "previous_close": previous_close[0].strip() if previous_close else None,
            "week_range": week_range[0].strip() if week_range else None,
            "one_year_return": one_year_return[0].strip() if one_year_return else None,
            "ytd_return": ytd_return[0].strip() if ytd_return else None,
            "pe_ratio": pe_ratio[0].strip() if pe_ratio else None,
            "earnings": earnings[0].strip() if earnings else None,
            "market_cap": market_cap[0].strip() if market_cap else None,
            "outstanding_shares": outstanding_shares[0].strip() if outstanding_shares else None,
            "price_sales": price_sales[0].strip() if price_sales else None,
            "div_indic_gross_yield": div_indic_gross_yield[0].strip() if div_indic_gross_yield else None,
        }

# URL Pattern Example: AAPL
# URL Pattern Example: ASX:WOW
# URL Pattern Example: ASX:IRE
class StockDetailScutify(Resource):
    def get(self, symbol):
        page = requests.get('https://www.scutify.com/service/sentiment.ashx?callType=getticker&ticker={symbol}'.format(symbol=symbol))
        json_data = json.loads(page.text)

        return {
            'bullish': json_data[0]['Bullish'],
            'bearish': json_data[0]['Bearish'],
        }


api.add_resource(StockDetailFT, '/ft/<string:symbol>')
api.add_resource(StockDetailBloomberg, '/bb/<string:symbol>')
api.add_resource(StockDetailScutify, '/sc/<string:symbol>')

cors = CORS(app)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8082)