from flask import Flask, request, jsonify
from flask_restful import Resource, Api
from flask_cors import CORS 

from lxml import html
import requests
import json
import base64

app = Flask(__name__)
api = Api(app)

class ProxyXplan(Resource):
    def get(self):
        headers = {
            'Origin': 'https://au02sp-t-xweb1.semiprod.iress.com.au',
            'Authorization': 'Basic ' + base64.b64encode(b"shabeeha:passw0rded").decode("utf-8"),
            'X-Xplan-App-Id': 'drVVTuCUC7oz9tu8wadq'

        }
        r = requests.get('https://au02sp-t-xweb1.semiprod.iress.com.au/pwmukmaster', headers=headers)

        import pdb
        pdb.set_trace()

        return {}

api.add_resource(ProxyXplan, '/')

cors = CORS(app)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8082)