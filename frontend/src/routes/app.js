export const GET_SECURITY_CODE = 'GET_SECURITY_CODE';
export const GET_SECURITY_EXCHANGE = 'GET_SECURITY_EXCHANGE';
export const GET_SECURITY_DETAILS_SUCCESS = 'GET_SECURITY_DETAILS_SUCCESS';


const appId = 'dGQNksQT8hquAI6PFQ2A';
const siteURL = 'http://au02-psutha-pc3.devel.iress.com.au:8080/ParthTrunk/';
const securitySearchURL = siteURL + 'resourceful/portfolio/security_search?search_term=%s&search_type=1';

const reqHeaders = new Headers({
  'Content-Type': 'application/json',
  'Authorization': 'Basic ' + btoa("parth:p"),
  // 'Authorization': 'Basic ' + btoa("shabeeha:passw0rded"),
  'X-Xplan-App-Id': appId
});

export const fetchSecurityData = () => {
  return (dispatch, getState) => {
        const state = getState();
        const code = state.app.code;
        const exchange = state.app.exchange;
        const url = securitySearchURL.replace("%s", code + "." + exchange);

        return new Promise((resolve) => {
            return fetch(url, {
              method: 'GET',
              headers: reqHeaders,
              mode: 'cors',
              cache: 'default'
            }).then(data => {
                data.json().then((resp) => {
                    dispatch({
                        type: GET_SECURITY_DETAILS_SUCCESS,
                        payload: resp[0]
                    })
                })
            })
        })
    }
};

export function setSecurityCode (value) {
  return {
    type    : GET_SECURITY_CODE,
    payload : value
  }
}

export function setSecurityExchange (value) {
  return {
    type    : GET_SECURITY_EXCHANGE,
    payload : value
  }
}

const ACTION_HANDLERS = {
    [GET_SECURITY_DETAILS_SUCCESS]: (state, action) => {
        return Object.assign({}, state, {
            security: action.payload
        })
    },
    [GET_SECURITY_CODE]: (state, action) => {
        return Object.assign({}, state, {
            code: action.payload
        })
    },
    [GET_SECURITY_EXCHANGE]: (state, action) => {
        return Object.assign({}, state, {
            exchange: action.payload
        })
    },
};


const initialState = {
    code: 'IRE',
    exchange: 'ASX',
    security: {
      security_id: 16786594
    },
};

export default function appReducer(state = initialState, action) {
    const handler = ACTION_HANDLERS[action.type]
    return handler ? handler(state, action) : state
}
