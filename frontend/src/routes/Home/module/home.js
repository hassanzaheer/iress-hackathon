export const GET_FT_STOCK_DETAILS_SUCCESS = 'GET_FT_STOCK_DETAILS_SUCCESS';
export const GET_BB_STOCK_DETAILS_SUCCESS = 'GET_BB_STOCK_DETAILS_SUCCESS';
export const GET_SC_STOCK_DETAILS_SUCCESS = 'GET_SC_STOCK_DETAILS_SUCCESS';
export const GET_CASH_AVAILABLE = 'GET_CASH_AVAILABLE';
export const PLACE_ORDER_COMPLETED = 'PLACE_ORDER_COMPLETED';
export const SET_BUY_VOLUMNE = 'SET_BUY_VOLUMNE';
export const SET_BUY_PRICE = 'SET_BUY_PRICE';
export const SET_SELL_VOLUMNE = 'SET_SELL_VOLUMNE';
export const SET_SELL_PRICE = 'SET_SELL_PRICE';

const ANALYSIS_API_URL = 'http://api-stock-scanner.hassanzaheer.com/';

export const fetchStockDetails = (secCode, source) => {
    return (dispatch) => {
        return new Promise((resolve) => {
            return fetch(`${ANALYSIS_API_URL}${secCode}`)
                .then(data => {
                    data.json().then((resp) => {
                        dispatch({
                            type: `GET_${source}_STOCK_DETAILS_SUCCESS`,
                            payload: resp
                        })
                    })
                })
        })
    }
}

const appId = 'dGQNksQT8hquAI6PFQ2A';
// const appId = 'drVVTuCUC7oz9tu8wadq'
const siteURL = 'http://au02-psutha-pc3.devel.iress.com.au:8080/ParthTrunk/';
// const siteURL = 'https://au02sp-t-xweb1.semiprod.iress.com.au/pwmukmaster/';
const multiOrderURL = siteURL + 'resourceful/trading/interface/iosplus/multi_order-v2';
const securitySearchURL = siteURL + 'resourceful/portfolio/security_search?search_term=%s&search_type=1';
const cashAvailableURL = siteURL + 'resourceful/portfolio/account/%accountId/position?proposed=false&aggregation.0=account&aggregation.1=cash_position&settled=false&include_unfilled_orders=true&fields.0=cash&fields.1=value';

const reqHeaders = new Headers({
  'Content-Type': 'application/json',
  'Authorization': 'Basic ' + btoa("parth:p"),
  // 'Authorization': 'Basic ' + btoa("shabeeha:passw0rded"),
  'X-Xplan-App-Id': appId
});

export const fetchCashAvailable = (accountId) => {
  const url = cashAvailableURL.replace("%accountId", accountId);
  return (dispatch) => {
        return new Promise((resolve) => {
            return fetch(url, {
              method: 'GET',
              headers: reqHeaders,
              mode: 'cors',
              cache: 'default'
            }).then(data => {
                data.json().then((resp) => {
                    let cashAvailable = 0;
                    resp.forEach((cashData) => {
                      if( cashData.cash) {
                        cashAvailable = cashData.value._val;
                      };
                    });
                    dispatch({
                        type: 'GET_CASH_AVAILABLE',
                        payload: cashAvailable
                    })
                })
            })
        })
    }
};

export function setBuyVolume (e, value = 0) {
  return {
    type    : SET_BUY_VOLUMNE,
    payload : value
  }
}

export function setBuyPrice (e, value = 0) {
  return {
    type    : SET_BUY_PRICE,
    payload : value
  }
}

export function setSellVolume (e, value = 0) {
  return {
    type    : SET_SELL_VOLUMNE,
    payload : value
  }
}

export function setSellPrice (e, value = 0) {
  return {
    type    : SET_SELL_PRICE,
    payload : value
  }
}

export const sendBuyOrder = () => {
  const url = multiOrderURL;
  return (dispatch, getState) => {
        const state = getState();
        const volume = state.home.buyVolume;
        const price = state.home.buyPrice;
        const securityId = state.app.security.security_id;

        const data = JSON.stringify({
            "params": [
                {
                    buy_or_sell: 'buy',
                    custom_columns: "",
                    destination: "DESK",
                    execution_instructions: "",
                    legs: null,
                    lifetime: null,
                    lifetime_type: "IGF",
                    order_giver: null,
                    order_taker: null,
                    percent: null,
                    portfolioid: "C1458",
                    price: price,
                    price_type: "1",
                    security_id: securityId,
                    subfund: "DEFAULT",
                    value: null,
                    volume: {_type: "BigDecimal", _val: volume}
                }
            ]
        });
        return new Promise((resolve) => {
            return fetch(url, {
              method: 'POST',
              headers: reqHeaders,
              mode: 'cors',
              cache: 'default',
              body: data
            }).then(data => {
                data.json().then((resp) => {
                    dispatch({
                        type: PLACE_ORDER_COMPLETED,
                        payload: resp
                    })
                })
            })
        })
    }
};

export const sendSellOrder = (data) => {
  const url = multiOrderURL;
  return (dispatch, getState) => {
        const state = getState();
        const volume = state.home.sellVolume;
        const price = state.home.sellPrice;
        const securityId = state.app.security.security_id;

        const data = JSON.stringify({
            "params": [
                {
                    buy_or_sell: 'sell',
                    custom_columns: "",
                    destination: "DESK",
                    execution_instructions: "",
                    legs: null,
                    lifetime: null,
                    lifetime_type: "IGF",
                    order_giver: null,
                    order_taker: null,
                    percent: null,
                    portfolioid: "C1458",
                    price: price,
                    price_type: "1",
                    security_id: securityId,
                    subfund: "DEFAULT",
                    value: null,
                    volume: {_type: "BigDecimal", _val: volume}
                }
            ]
        });

        return new Promise((resolve) => {
            return fetch(url, {
              method: 'POST',
              headers: reqHeaders,
              mode: 'cors',
              cache: 'default',
              body: data
            }).then(data => {
                data.json().then((resp) => {
                    dispatch({
                        type: PLACE_ORDER_COMPLETED,
                        payload: resp
                    })
                })
            })
        })
    }
};

const ACTION_HANDLERS = {
    [GET_FT_STOCK_DETAILS_SUCCESS]: (state, action) => {
        return Object.assign({}, state, {
            ftStockData: action.payload
        })
    },
    [GET_BB_STOCK_DETAILS_SUCCESS]: (state, action) => {
        return Object.assign({}, state, {
            bbStockData: action.payload
        })
    },
    [GET_SC_STOCK_DETAILS_SUCCESS]: (state, action) => {
        return Object.assign({}, state, {
            scStockData: action.payload
        })
    },
    [GET_CASH_AVAILABLE]: (state, action) => {
        return Object.assign({}, state, {
            cashAvailable: action.payload
        })
    },
    [SET_BUY_VOLUMNE]: (state, action) => {
        return Object.assign({}, state, {
            buyVolume: action.payload
        })
    },
    [SET_BUY_PRICE]: (state, action) => {
        return Object.assign({}, state, {
            buyPrice: action.payload
        })
    },
    [SET_SELL_VOLUMNE]: (state, action) => {
        return Object.assign({}, state, {
            sellVolume: action.payload
        })
    },
    [SET_SELL_PRICE]: (state, action) => {
        return Object.assign({}, state, {
            sellPrice: action.payload
        })
    },
};

const initialState = {
    selectedStock: 'IRE:ASX',
    ordersData: {},
    ftStockData: {
      recommendations: {}
    },
    bbStockData: {},
    scStockData: {},
    cashAvailable: '0',
    buyVolume: 0,
    buyPrice: 0,
    sellVolume: 0,
    sellPrice: 0,
    securityId: 16786594,
};

export default function homeReducer(state = initialState, action) {
    const handler = ACTION_HANDLERS[action.type]
    return handler ? handler(state, action) : state
}








