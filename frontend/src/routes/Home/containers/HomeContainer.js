import {
    connect
} from 'react-redux'
import {
    fetchStockDetails,
    fetchCashAvailable,
    sendBuyOrder,
    sendSellOrder,
    setBuyVolume,
    setBuyPrice,
    setSellVolume,
    setSellPrice,
} from '../module/home'
import HomeView from '../components/HomeView'

const mapDispatchToProps = {
    fetchStockDetails,
    fetchCashAvailable,
    sendBuyOrder,
    sendSellOrder,
    setBuyPrice,
    setBuyVolume,
    setSellVolume,
    setSellPrice,
};

const mapStateToProps = (state) => ({
    home: state.home,
    app: state.app
});

export default connect(mapStateToProps, mapDispatchToProps)(HomeView)
