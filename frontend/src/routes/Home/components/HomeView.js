import React from 'react'
import PropTypes from 'prop-types'
import './HomeView.scss'

import OrdersComponent from '../../../components/OrdersComponent'
import StockDetailsComponent from '../../../components/StockDetailsComponent'
import MarketDataComponent from '../../../components/MarketDataComponent'
import AnalysisComponent from '../../../components/AnalysisComponent'

import AppBar from 'material-ui/AppBar'
import {
  Toolbar,
  ToolbarGroup,
  ToolbarSeparator,
  ToolbarTitle
} from 'material-ui/Toolbar'
import TextField from 'material-ui/TextField'

class HomeView extends React.Component {

  static propTypes = {
    fetchStockDetails: PropTypes.func.isRequired,
    fetchCashAvailable: PropTypes.func.isRequired,
    sendBuyOrder: PropTypes.func.isRequired,
    sendSellOrder: PropTypes.func.isRequired,
    setBuyPrice: PropTypes.func.isRequired,
    setBuyVolume: PropTypes.func.isRequired,
    setSellVolume: PropTypes.func.isRequired,
    setSellPrice: PropTypes.func.isRequired,
    home: PropTypes.object.isRequired,
    app: PropTypes.object.isRequired
  }

  constructor() {
    super()
  }

  componentWillMount() {
    this.props.fetchCashAvailable('724')
  }

  render() {
    return (
      <div>
        <div className='hidden-xs-down'>
          <AppBar
            title='IRESSallegro'
            iconStyleLeft={{
              "display": "none"
            }}
            style={{
              "backgroundColor": '#3F51B5'
            }}
            children={
              <TextField
                hintText='Search Security'
                type='text'
                style={{
                  "marginTop": "5px",
                  "color": "white",
                  'marginLeft': '10px',
                  'width': 'calc(100% - 160px)',
                  'fullWidth': true,
                }}
                inputStyle={{
                  "color": "white"
                }}
                hintStyle={{
                  'color': 'white',
                }}
              />
            }
          />
        </div>

        <div className='hidden-sm-up'>
          <AppBar
            titleStyle={{
              'flex': 'none'
            }}
            iconStyleLeft={{
              "display": "none"
            }}
            children={
              <TextField
                hintText='Search Security'
                type='text'
                style={{
                  "marginTop": "5px",
                  "color": "white",
                  'marginLeft': '10px',
                  'width': 'calc(100% - 20px)',
                  'fullWidth': true,
                }}
                inputStyle={{
                  "color": "white"
                }}
                hintStyle={{
                  'color': 'white',
                }}
              />
            }
          />
        </div>


        <div className='container-fluid'>

          <StockDetailsComponent security={this.props.app.security}/>

          <MarketDataComponent />

          <OrdersComponent cashAvailable={this.props.home.cashAvailable}
                           sendBuyOrder={this.props.sendBuyOrder}
                           sendSellOrder={this.props.sendSellOrder}
                           setBuyPrice={this.props.setBuyPrice}
                           setBuyVolume={this.props.setBuyVolume}
                           setSellVolume={this.props.setSellVolume}
                           setSellPrice={this.props.setSellPrice}
          />

          <AnalysisComponent
            ftStockData={this.props.home.ftStockData}
            bbStockData={this.props.home.bbStockData}
            scStockData={this.props.home.scStockData}
          />

        </div>

      </div>
    )
  }
}

export default HomeView
