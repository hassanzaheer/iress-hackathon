import React from 'react'

export const StockDetailsComponent = (props) => (
  <div className='row'>
    <div className='col-sm-12' style={{ padding: '12px', 'textAlign': 'left' }}>
      <h1 className='text-left' style={{'display': 'inline-block'}}>{props.security.code}:{props.security.exchange}</h1>
      <h2 className='text-left' style={{'display': 'inline-block', 'marginLeft': '10px'}}>{props.security.description}</h2>
    </div>
  </div>
)

export default StockDetailsComponent
