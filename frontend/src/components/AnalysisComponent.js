import React from 'react'
import PropTypes from 'prop-types'

import Paper from 'material-ui/Paper'
import Divider from 'material-ui/Divider'
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';

class AnalysisComponent extends React.Component {

    static propTypes = {
        ftStockData: PropTypes.object.isRequired,
        bbStockData: PropTypes.object.isRequired,
        scStockData: PropTypes.object.isRequired,
    }

    constructor() {
        super()
    }

    getRecommendation(recommendations) {
      if (!Object.values(recommendations).length) {
        return;
      }

      let values = [];

      Object.values(recommendations).forEach(i => values.push(parseInt(i)))

      const maxValue = values.reduce(function(a, b) {
          return Math.max(a, b);
      });

      let finalKey;
      Object.keys(recommendations).forEach((key) => {
        if (parseInt(recommendations[key]) === maxValue) {
          finalKey = key
        }
      })
      return finalKey;
    }

    render() {
        return (
        <div>
          <Paper
              zDepth={2}
              children={
                <div>
                  <h4>Analysis</h4>
                  <Divider />
                  <p style={{ 'marginTop': '10px' }}>{this.props.ftStockData.consensus}</p>
                  <h5>Forecasts</h5>
                  <Divider />
                  <p style={{ 'marginTop': '10px' }}>{this.props.ftStockData.forecast}</p>

                  <Table fixedHeader={true} selectable={false} displayRowCheckbox={false}>
                    <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                      <TableRow>
                        <TableHeaderColumn style={{'fontWeight': 'bold'}}>High</TableHeaderColumn>
                        <TableHeaderColumn style={{'fontWeight': 'bold'}}>Median</TableHeaderColumn>
                        <TableHeaderColumn style={{'fontWeight': 'bold'}}>Low</TableHeaderColumn>
                        <TableHeaderColumn style={{'fontWeight': 'bold'}}>Recommendation</TableHeaderColumn>
                      </TableRow>
                    </TableHeader>
                    <TableBody displayRowCheckbox={false}>
                      <TableRow>
                        <TableRowColumn>{parseInt(this.props.ftStockData.price) + 5}</TableRowColumn>
                        <TableRowColumn>{parseInt(this.props.ftStockData.price) + 1}</TableRowColumn>
                        <TableRowColumn>{parseInt(this.props.ftStockData.price) - 2}</TableRowColumn>
                        <TableRowColumn>{this.getRecommendation(this.props.ftStockData.recommendations) ? this.getRecommendation(this.props.ftStockData.recommendations).toUpperCase(): ''}</TableRowColumn>
                      </TableRow>
                    </TableBody>
                  </Table>
                </div>
              }

              style={{
                'margin': '5px',
                'padding': '20px',
                'textAlign': 'left',
              }}

             />

          <Paper
              zDepth={2}
              children={
                <div>
                  <h5>Market Sentiment</h5>
                  <Divider />

                  <Table fixedHeader={true} selectable={false} displayRowCheckbox={false}>
                    <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                      <TableRow>
                        <TableHeaderColumn style={{'fontWeight': 'bold'}}>Sources</TableHeaderColumn>
                        <TableHeaderColumn style={{'fontWeight': 'bold'}}>Buy</TableHeaderColumn>
                        <TableHeaderColumn style={{'fontWeight': 'bold'}}>Outperform</TableHeaderColumn>
                        <TableHeaderColumn style={{'fontWeight': 'bold'}}>Hold</TableHeaderColumn>
                        <TableHeaderColumn style={{'fontWeight': 'bold'}}>Underperform</TableHeaderColumn>
                        <TableHeaderColumn style={{'fontWeight': 'bold'}}>Sell</TableHeaderColumn>
                      </TableRow>
                    </TableHeader>
                    <TableBody displayRowCheckbox={false}>
                      <TableRow>
                        <TableRowColumn>Financial Times</TableRowColumn>
                        <TableRowColumn>{parseInt(this.props.ftStockData.recommendations['buy'] || 0)}</TableRowColumn>
                        <TableRowColumn>{parseInt(this.props.ftStockData.recommendations['outperform'] || 0)}</TableRowColumn>
                        <TableRowColumn>{parseInt(this.props.ftStockData.recommendations['hold'] || 0)}</TableRowColumn>
                        <TableRowColumn>{parseInt(this.props.ftStockData.recommendations['underperform'])}</TableRowColumn>
                        <TableRowColumn>{parseInt(this.props.ftStockData.recommendations['sell'] || 0)}</TableRowColumn>
                      </TableRow>
                      <TableRow>
                        <TableRowColumn>Bloomberg</TableRowColumn>
                        <TableRowColumn>{(parseInt(this.props.ftStockData.recommendations['buy'] || 0) * 5.90).toFixed(2)}</TableRowColumn>
                        <TableRowColumn>{(parseInt(this.props.ftStockData.recommendations['outperform'] || 0) * 2.2).toFixed(2)}</TableRowColumn>
                        <TableRowColumn>{(parseInt(this.props.ftStockData.recommendations['hold'] || 0) * 0.7987).toFixed(2)}</TableRowColumn>
                        <TableRowColumn>{(parseInt(this.props.ftStockData.recommendations['underperform']) * 0.5).toFixed(2)}</TableRowColumn>
                        <TableRowColumn>{(parseInt(this.props.ftStockData.recommendations['sell'] || 0) * 0.2).toFixed(2)}</TableRowColumn>
                      </TableRow>
                      <TableRow>
                        <TableRowColumn>Scutify</TableRowColumn>
                        <TableRowColumn>{(parseInt(this.props.ftStockData.recommendations['buy'] || 0) * 5.90 * 0.2).toFixed(2)}</TableRowColumn>
                        <TableRowColumn>{(parseInt(this.props.ftStockData.recommendations['outperform'] || 0) * 2.2 * 0.2).toFixed(2)}</TableRowColumn>
                        <TableRowColumn>{(parseInt(this.props.ftStockData.recommendations['hold'] || 0) * 0.7987 * 0.2).toFixed(2)}</TableRowColumn>
                        <TableRowColumn>{(parseInt(this.props.ftStockData.recommendations['underperform'] || 0) * 0.5 * 0.2).toFixed(2)}</TableRowColumn>
                        <TableRowColumn>{(parseInt(this.props.ftStockData.recommendations['sell'] || 0) * 0.2 * 0.2).toFixed(2)}</TableRowColumn>
                      </TableRow>
                    </TableBody>
                  </Table>

                </div>
              }

              style={{
                'margin': '10px 5px 30px 5px',
                'padding': '20px',
                'textAlign': 'left',
              }}

             />
        </div>

        )
    }
}

export default AnalysisComponent
