import React from 'react'
import PropTypes from 'prop-types'

import Paper from 'material-ui/Paper'
import RaisedButton from 'material-ui/RaisedButton'
import Divider from 'material-ui/Divider'
import TextField from 'material-ui/TextField'

export class OrdersComponent  extends React.Component {
  static propTypes = {
    cashAvailable: PropTypes.string.isRequired,
    sendBuyOrder: PropTypes.func.isRequired,
    sendSellOrder: PropTypes.func.isRequired,
    setBuyPrice: PropTypes.func.isRequired,
    setBuyVolume: PropTypes.func.isRequired,
    setSellPrice: PropTypes.func.isRequired,
    setSellVolume: PropTypes.func.isRequired,
  }

  onBuyPriceChange() {

  }

  render() {
    return <div className='row'>
      <div className='col-sm-12'>
        <Paper
          zDepth={2}
          style={{
            margin: '5px',
            padding: '20px',
          }}
          children={
            <div style={{'textAlign': 'left'}}>
              <h4 className='text-left label-1' style={{'display': 'inline-block', 'marginRight': 10}}>Trading Account: HASSAN1 - </h4>
              <h5 className='text-left label-1' style={{'display': 'inline-block'}}>Cash Available: {this.props.cashAvailable}</h5>
              <Divider style={{'margin': '10px 0 10px 0'}}/>
              <div>
                <TextField
                  hintText='Quantity'
                  type='number'
                  floatingLabelText='Enter Quantity'
                  className='textfield-1'
                  onChange={this.props.setBuyVolume}
                  style={{
                    'marginRight': '10px'
                  }}
                />
                <TextField
                  hintText='Price'
                  type='number'
                  floatingLabelText='Enter Price'
                  className='textfield-1'
                  onChange={this.props.setBuyPrice}
                  style={{
                    'marginRight': '10px'
                  }}
                />
                <RaisedButton label='Buy' backgroundColor='green' labelColor='#fff' className='btn-1'
                              onTouchTap={this.props.sendBuyOrder}/>
              </div>
              <div>
                <TextField
                  hintText='Quantity'
                  type='number'
                  floatingLabelText='Enter Quantity'
                  className='textfield-1'
                  onChange={this.props.setSellVolume}
                  style={{
                    'marginRight': '10px'
                  }}
                  defaultValue={1000}
                />
                <TextField
                  hintText='Price'
                  type='number'
                  floatingLabelText='Enter Price'
                  className='textfield-1'
                  onChange={this.props.setSellPrice}
                  style={{
                    'marginRight': '10px'
                  }}
                />
                <RaisedButton label='Sell' backgroundColor='red' labelColor='#fff' className='btn-1'
                              onTouchTap={this.props.sendSellOrder}/>
              </div>
            </div>
          }
        />
      </div>
    </div>
  }

}

export default OrdersComponent
