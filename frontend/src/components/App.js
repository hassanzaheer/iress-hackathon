import React from 'react'
import { browserHistory, Router } from 'react-router'
import { Provider } from 'react-redux'
import PropTypes from 'prop-types'

import {
  indigo500,
  indigo200,
} from 'material-ui/styles/colors'
import injectTapEventPlugin from 'react-tap-event-plugin'
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import {
    setSecurityCode,
    setSecurityExchange,
    fetchSecurityData,
} from '../routes/app'
import {
    fetchStockDetails
} from '../routes/Home/module/home'

const muiTheme = getMuiTheme({
  palette: {
    primary1Color: indigo500,
  },
})

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

class App extends React.Component {
  static propTypes = {
    store: PropTypes.object.isRequired,
    routes: PropTypes.object.isRequired,
  }

  shouldComponentUpdate () {
    return false
  }

  componentDidMount() {
    const code = getParameterByName('code');
    const exchange = getParameterByName('exchange');

    this.props.store.dispatch(setSecurityCode(code));
    this.props.store.dispatch(setSecurityExchange(exchange));
    this.props.store.dispatch(fetchSecurityData());
    this.props.store.dispatch(fetchStockDetails(code + ':' + exchange, 'FT'))
  }

  render () {
    return (
      <Provider store={this.props.store}>
        <MuiThemeProvider muiTheme={muiTheme}>
          <div style={{ height: '100%' }}>
            <Router history={browserHistory} children={this.props.routes} />
          </div>
        </MuiThemeProvider>
      </Provider>
    )
  }
}

injectTapEventPlugin()

export default App
