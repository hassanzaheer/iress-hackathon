var exchanges = ['ASX', 'LSE', 'FRA', 'NSQ'];
var mappedexchanges = ['ASX', 'LSE', 'FRA', 'NAS'];

chrome.extension.sendMessage({}, function(response) {
	var readyStateCheckInterval = setInterval(function() {
	if (document.readyState === "complete") {
		clearInterval(readyStateCheckInterval);

		function findByText(rootElement, exchanges){
			var filter = {
				acceptNode: function(node){
					// look for nodes that are text_nodes and include the following string.
					var accept = false;
					if(node.nodeType === document.TEXT_NODE){
						for(var i = 0; i < exchanges.length; i++) {
							accept = node.nodeValue.includes(":" + exchanges[i]) || node.nodeValue.includes(exchanges[i] + ":");
							if (accept){
								break;
							}
						}
					}
					return accept ? NodeFilter.FILTER_ACCEPT : NodeFilter.FILTER_REJECT;
				}
			};
			var nodes = [];
			var walker = document.createTreeWalker(rootElement, NodeFilter.SHOW_TEXT, filter, false);
			while(walker.nextNode()){
			   //give me the element containing the node
			   nodes.push(walker.currentNode.parentNode);
			}
			return nodes;
		}


		var excludedTags = ['SCRIPT', 'HEAD'];

		var nodes = findByText(document.body, exchanges);
		var securityTextNodes = [];

		//then do what you will with nodes[];
		for(var i = 0; i < nodes.length; i++){
			var node = nodes[i];
			if (excludedTags.indexOf(node.tagName) == -1) {
				var security = node.textContent;
				var securityData = security.split(":");
				var code = securityData[0];
				var exchange = securityData[1];
				code = code.replace(/\(|\)/g,'');
				exchange = exchange.replace(/\(|\)/g,'');

				if (mappedexchanges.indexOf(code) > -1) {
					code = securityData[1];
					exchange = securityData[0];
					code = code.replace(/\(|\)/g,'');
					exchange = exchange.replace(/\(|\)/g,'');
				}



				securityTextNodes.push(node);

				var btnNode = getBtn(code, exchange);
				node.appendChild(btnNode);
			}
		}



		$.get(chrome.extension.getURL('/src/inject/popup.html'), function(data) {
			$(data).appendTo('body');
			bindFormActions();
		});
	}
	}, 10);
});

function onOrderBtnClick(e) {
	e.stopPropagation();
	e.preventDefault();

	var btn = e.currentTarget;
	var code = btn.getAttribute('data-code');
	var exchange = btn.getAttribute('data-exchange');
	exchange = mappedexchanges[exchanges.indexOf(exchange)];

	var pos = btn.getBoundingClientRect();
	var width = $(btn).width();
	var height = $(btn).height();

	var left = pos.left + width;
	var top = pos.top + height;

	$(".iress-allegro-popup .code").html(code);
	$(".iress-allegro-popup .exchange").html(exchange);

	$(".iress-allegro-container").show();
	$(".iress-allegro-backdrop").show();
	$(".iress-allegro-popup").css({top: 0, right: -800,height: window.innerHeight});

	$("body").css("overflow", "hidden");

	var url = 'http://au02-psutha-pc3.devel.iress.com.au:8081/?code=' + code + "&exchange=" + exchange;
	$(".iress-allegro-popup-iframe").attr('src', url);

	$(".iress-allegro-popup").animate({
		right: 0,
  	}, 500, function() {
		// Animation complete.
  	});


	getSecurityData(code, exchange);
}

function getBtn(code, exchange) {
	var img = document.createElement('img');
	img.src = chrome.extension.getURL('/src/inject/logo.png');

	var btn = document.createElement('button');
	btn.className = 'iress-allegro-order-btn';
	btn.onclick = onOrderBtnClick;
	btn.setAttribute("data-code", code);
	btn.setAttribute("data-exchange", exchange);

	btn.appendChild(img);

	return btn;
}

function bindFormActions() {
	$(".iress-allegro-container").on('click', function (e) {
		e.stopPropagation();
		$(".iress-allegro-container").hide();
		$(".iress-allegro-backdrop").hide();
		$("body").css("overflow", "auto");
    });
	$(".iress-allegro-popup").on('click', function (e) {
		e.stopPropagation();
    });
}

function sendBuyOrder(security_id, quantity, price) {
	var type = 'send_buy_order';
	var data = {
		security_id: security_id,
		quantity: quantity,
		price: price,
		buy_or_sell: 'buy'
	};

	return sendMessage(type, data);
}

function getSecurityData(code, exchange) {
	var type = 'security_data';
	var data = {
		code: code,
		exchange: exchange,
	};

	return sendMessage(type, data);
}


/**   background message handlers **/
function orderSent(data) {
	var orderNumber = data.order_number;
}

function securityDataReceived(data) {
	var securityId = data[0].security_id;
	$(".iress-allegro-popup #hidden-security-id").val(securityId);
	$(".iress-allegro-popup").show();

}
























var port = chrome.runtime.connect({name: "iress_allergo"});
port.onMessage.addListener(function(msg) {
	var data = JSON.parse(msg.data);
	switch (msg.type) {
		case "order_placed":
			orderSent(data);
			break;
		case "security_data":
			securityDataReceived(data);
			break;
	}
});

function sendMessage(type, data) {
	return new Promise(function(resolve, reject) {
		port.postMessage({type: type, data: data, id: getMessageId()});
	});
}

var nextMsgId = 0;
function getMessageId() {
	return ++nextMsgId;
}