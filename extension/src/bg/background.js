chrome.runtime.onConnect.addListener(function(port) {
    port.onMessage.addListener(function (msg) {
        switch (msg.type) {
            case 'security_data':
                getSecurityData(msg.data).then(function (response) {
                    port.postMessage({
                        type: 'security_data',
                        data: response
                    });
                });
                break;
            case 'send_buy_order':
                sendMultiOrder(msg.data).then(function(response) {
                    port.postMessage({
                        type: 'order_placed',
                        data: response
                    });
                });
                break;
        }
    });
});


var appId = 'dGQNksQT8hquAI6PFQ2A';
var siteURL = 'http://au02-psutha-pc3.devel.iress.com.au:8080/ParthTrunk/';
var multiOrderURL = siteURL + 'resourceful/trading/interface/iosplus/multi_order-v2';
var securitySearchURL = siteURL + 'resourceful/portfolio/security_search?search_term=%s&search_type=1';

function sendXHR(method, url, data) {
    return new Promise(function(resolve, reject) {
        var xhr = new XMLHttpRequest();
        xhr.open(method, url, true);
        xhr.onreadystatechange = function() {
          if (xhr.readyState == 4) {
              resolve(xhr.responseText);
          }
        };
        xhr.setRequestHeader("Content-Type", 'application/json');
        xhr.setRequestHeader("Authorization", "Basic " + btoa("parth:p"));
        xhr.setRequestHeader("X-Xplan-App-Id", appId);
        xhr.send(JSON.stringify(data));
    });
}

function loginToXplanSite(appId) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", 'http://au02-psutha-pc3.devel.iress.com.au:8080/ParthTrunk/home', true);
    xhr.onreadystatechange = function() {
      if (xhr.readyState == 4) {

      }
    };
    xhr.setRequestHeader("Authorization", "Basic " + btoa("parth:p"));
    xhr.setRequestHeader("X-Xplan-App-Id", appId);
    xhr.send();
}

loginToXplanSite(appId);

function sendMultiOrder(data) {
    var data = {
        "params": [
            {
                buy_or_sell: data.buy_or_sell,
                custom_columns: "",
                destination: "DESK",
                execution_instructions: "",
                legs: null,
                lifetime: null,
                lifetime_type: "IGF",
                order_giver: null,
                order_taker: null,
                percent: null,
                portfolioid: "C1458",
                price: data.price,
                price_type: "1",
                security_id: data.security_id,
                subfund: "DEFAULT",
                value: null,
                volume: {_type: "BigDecimal", _val: data.quantity}
            }
        ]
    };

    return sendXHR('POST', multiOrderURL, data);
}

function getSecurityData(data) {
    var url = securitySearchURL.replace("%s", data.code + "." + data.exchange);
    return sendXHR('GET', url, data);
}

